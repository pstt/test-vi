# ************************************************************
# Sequel Pro SQL dump
# Версия 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Адрес: 127.0.0.1 (MySQL 5.7.19-0ubuntu0.16.04.1)
# Схема: vi
# Время создания: 2017-11-10 15:54:32 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Дамп таблицы courier
# ------------------------------------------------------------

DROP TABLE IF EXISTS `courier`;

CREATE TABLE `courier` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(500) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

LOCK TABLES `courier` WRITE;
/*!40000 ALTER TABLE `courier` DISABLE KEYS */;

INSERT INTO `courier` (`id`, `name`)
VALUES
	(2,'Львович Глеб Викторович'),
	(3,'Урбанович Иван Иванович'),
	(4,'Петров Петр Иванович'),
	(5,'Лебедев Илья Сергеевич'),
	(6,'Измайлов Марат Александрович'),
	(7,'Иванов Иван Иванович'),
	(8,'Сироткин Геннадий Вячеславович'),
	(9,'Сидоров Иван Иванович'),
	(10,'Глебов Глеб Глебович'),
	(11,'Заболотный Сергей Сергеевич');

/*!40000 ALTER TABLE `courier` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы region
# ------------------------------------------------------------

DROP TABLE IF EXISTS `region`;

CREATE TABLE `region` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(500) NOT NULL DEFAULT '',
  `to_from` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

LOCK TABLES `region` WRITE;
/*!40000 ALTER TABLE `region` DISABLE KEYS */;

INSERT INTO `region` (`id`, `name`, `to_from`)
VALUES
	(2,'Санкт-Петербург',9),
	(3,'Уфа',24),
	(4,'Нижний Новгород',9),
	(5,'Владимир',4),
	(6,'Кострома',10),
	(7,'Екатеринбург',28),
	(8,'Ковров',3),
	(9,'Воронеж',9),
	(10,'Самара',10),
	(11,'Астрахань',36);

/*!40000 ALTER TABLE `region` ENABLE KEYS */;
UNLOCK TABLES;


# Дамп таблицы schedule
# ------------------------------------------------------------

DROP TABLE IF EXISTS `schedule`;

CREATE TABLE `schedule` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `courier_id` int(11) NOT NULL,
  `region_id` int(11) NOT NULL,
  `datetime_to` int(128) NOT NULL,
  `datetime_from` int(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

LOCK TABLES `schedule` WRITE;
/*!40000 ALTER TABLE `schedule` DISABLE KEYS */;

INSERT INTO `schedule` (`id`, `courier_id`, `region_id`, `datetime_to`, `datetime_from`)
VALUES
	(1,3,2,1510315200,1510347600),
	(2,5,4,1510322400,1510387200),
	(7,3,3,1510390800,1510563600);

/*!40000 ALTER TABLE `schedule` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
