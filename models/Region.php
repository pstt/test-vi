<?php

namespace vi\models;

use vi\models\BaseModel;

/**
 * Description of Region
 *
 * @author titovskiy
 */
class Region extends BaseModel{
    public $name;
    public $lasting;
    
    function getName() {
        return $this->name;
    }

    function getLasting() {
        return $this->lasting;
    }

    function setName($name) {
        $this->name = $name;
    }

    function setLasting($lasting) {
        $this->lasting = $lasting;
    }
}
