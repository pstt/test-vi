<?php

namespace vi\models;

/**
 * Description of BaseModel
 *
 * @author titovskiy
 */
class BaseModel {
    static public function getDb()
    {
        $config = parse_ini_file('conf.ini');
        $dsn = "{$config['driver']}:host={$config['host']};dbname={$config['schema']};charset=utf8";
        return new \PDO($dsn, $config['user'], $config['password']);
    }
    
    static public function getAll($table)
    {
        $db = BaseModel::getDb();
        $query = $db->query('SELECT * FROM '.$table);
        $regions = $query->fetchAll(\PDO::FETCH_ASSOC);
        return $regions;
    }
    
    static public function getFromBy($select, $from, array $by)
    {
        $db = BaseModel::getDb();
        $sql = 'SELECT '. $select . ' FROM '.$from.' WHERE ';
        $keys = array_keys($by);
        foreach ($by as $key=>$val){
            if ($keys['0'] == $key){
                $sql .= $key.$val;
            } else {
                $sql .= ','.$key.$val;
            }
            
        }
        $query = $db->query($sql);
        $result = $query ? $query->fetchAll(\PDO::FETCH_ASSOC) : null;
        return (!empty($result)) ? $result : false;
    }
    
    static public function save($table, $values)
    {
        $db = BaseModel::getDb();
        $insert = '';
        $sql = 'INSERT INTO '. $table . '(';
        $keys = array_keys($values);
        foreach ($values as $key=>$val){
            if ($keys['0'] == $key){
                $sql .= $key;
                $insert .= $val;
            } else {
                $sql .= ',' . $key;
                $insert .= ',' . $val;
            }
        }
        $sql .= ') VALUES(' . $insert .')';
        $query = $db->prepare($sql);
        $result = true ? $query->execute() : false;
        return $result;
    }
    
    static public function execSql($sql) 
    {
        $db = BaseModel::getDb();
        $query = $db->query($sql);
        $result = true ? $query->fetchAll(\PDO::FETCH_ASSOC) : false;
        return $result;
    }
    
    static public function execInsertSql($sql) 
    {
        $db = BaseModel::getDb();
        $query = $db->prepare($sql);
        $result = true ? $query->execute() : false;
        return $result;
    }
}
