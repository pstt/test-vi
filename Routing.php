<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace vi;

/**
 * Description of Routinng
 *
 * @author titovskiy
 */

class Routing
{
    static function explodee($request) {
        $name = '';
        if ($controller = explode('-', $request)){
            foreach ($controller as $key=>$value) {
                $name .= ucfirst($value);
            }
            return lcfirst($name);
        } else return $request;
    }
    
    static function execute() 
    {
            $controllerName = 'Index';	
            $actionName = 'index';
            $piecesOfUrl = explode('/', $_SERVER['REQUEST_URI']);

            if (!empty($piecesOfUrl[1]) && empty($piecesOfUrl[2])) 
            {
                $actionName = Routing::explodee($piecesOfUrl[1]);   
            }
            if (!empty($piecesOfUrl[1]) && !empty($piecesOfUrl[2])) 
            {
                $controllerName = Routing::explodee($piecesOfUrl[1]);   
            }
            if (!empty($piecesOfUrl[2]))
            {
                $actionName = Routing::explodee($piecesOfUrl[2]);
            }
            $controllerName = ucfirst($controllerName) . 'Controller';
            $actionName = $actionName . 'Action';
            $fileWithController = $controllerName . '.php';
            $fileWithControllerPath = "controllers/" . $fileWithController;
            if (file_exists($fileWithControllerPath))
            {
                    include $fileWithControllerPath;
            } else {
                header("location: /not-found");
            }
            $controller = new $controllerName;
            $action = $actionName;
            if (method_exists($controller, $action))
            {
                    call_user_func(array($controller, $action), $piecesOfUrl);
            } else {
                header("location: /not-found");
                    echo "<h1>NOT FOUND! 404</h1> <br> <a class href='/'>Домой</a>";
            }
    }	
}
?>
