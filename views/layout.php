<!DOCTYPE html>
<html lang="RU">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <style>
        body {
        padding-top: 5rem;
      }
      .starter-template {
        padding: 3rem 1.5rem;
      }
      .nav-item:hover {
          background-color: gray;
      }
      
        .ACCEPTABLE{
                border: 2px solid #4cae4c;

        }

        .UNACCEPTABLE{
                border: 2px solid red;
        }

        .REQUIRED{
                border: 2px solid #d9534f;
        }

        .NOREQUIRED{
                border: 2px solid #4cae4c;
        }
    </style>
    <title><?= $data['title']; ?></title>
</head>
<body>

    <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
      <a class="navbar-brand" href="/">Курьерская доставка</a>
      

      <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="/index/schedule">Расписание поездок <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="/index/form">Форма внесения данных <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="/index/upload">Загрузка данных за предыдущие периоды <span class="sr-only">(current)</span></a>
          </li>
          
        </ul>
      </div>
    </nav>
    <div class="container">

      <div class="starter-template">
        <?php
        $filename = 'views/'.$template . '.php';
        file_exists($filename) ? include $filename : header('Location: "/not-found');
        ?>
      </div>

    </div>
    
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/validate.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <?= $data['js']; ?>
</body>
</html>


