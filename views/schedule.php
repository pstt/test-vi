<?= (isset($data['resultSave']) && !empty($data['resultSave'])) ? $data['resultSave'] : null ?>
<form class="" id="form-schedule" method="POST">
  <div class="form-group">
    <label for="region">Регион</label>
    <select name='region' class="form-control" id="region" 
            data-rule-required='[{"key":"!regexp","value":"\\d"}]'>
        <option disabled selected value> -- Выберите -- </option>
        <?php foreach ($data['regions'] as $region) {
            echo '<option value='.$region['id'].'>'.$region['name'].'</option>';
        }?>
    </select>
  </div>
  <div class="form-group ">
    <label for="dateFrom">С</label>
    <input type="text" name='dateFrom' class="form-control" id="dateFrom" placeholder="ГГГГ-ММ-ДД"
           data-rule-required='[{"key":"!regexp","value":"^[2][0-1][0-1][0-9]-[0-1][0-9]-[0-3][0-9]$"}]'>
  </div>
  <div class="form-group">
    <input type="text" name='timeFrom' class="form-control" id="timeFrom" placeholder="ЧЧ:ММ 24-часовой формат"
           data-rule-required='[{"key":"!regexp","value":"^[0-2][0-9]:[0-9]{2,2}$"}]'>
  </div>
  <div class="form-group">
    <label for="dateTo">ПО</label>
    <input type="text" name='dateTo' class="form-control" id="dateTo" placeholder="ГГГГ-ММ-ДД"
           data-rule-required='[{"key":"!regexp","value":"^[2][0-1][0-1][0-9]-[0-1][0-9]-[0-3][0-9]$"}]'>
  </div>
  <div class="form-group">
    <input type="text" name='timeTo' class="form-control" id="timeTo" placeholder="ЧЧ:ММ 24-часовой формат"
           data-rule-required='[{"key":"!regexp","value":"^[0-2][0-9]:[0-9]{2,2}$"}]'>
  </div>
  
  
</form>
<div class="form-group" id="result"></div>