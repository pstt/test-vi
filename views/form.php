<?= (isset($data['resultSave']) && !empty($data['resultSave'])) ? $data['resultSave'] : null ?>
<form method="POST" id="form">
  <div class="form-group">
    <label for="region">Регион</label>
    <select name='region' class="form-control" id="region" 
            data-rule-required='[{"key":"!regexp","value":"\\d"}]'>
        <option disabled selected value> -- Выберите -- </option>
        <?php foreach ($data['regions'] as $region) {
            echo '<option value='.$region['id'].'>'.$region['name'].'</option>';
        }?>
    </select>
  </div>
  <div class="form-group">
    <label for="date">Дата выезда из Москвы</label>
    <input type="text" name='date' class="form-control" id="date" placeholder="ГГГГ-ММ-ДД"
           data-rule-required='[{"key":"!regexp","value":"^[2][0-1][0-1][0-9]-[0-1][0-9]-[0-3][0-9]$"}]'>
    
  </div>
<div class="form-group">
    <label for="time">Время выезда из Москвы</label>
    <input type="text" name='time' class="form-control" id="time" placeholder="ЧЧ:ММ 24-часовой формат"
           data-rule-required='[{"key":"!regexp","value":"^[0-2][0-9]:[0-9]{2,2}$"}]'>
    
  </div>
  <div class="form-group">
    <label for="courier">Курьер</label>
    <select name='courier' type="email" class="form-control" id="courier" 
            data-rule-required='[{"key":"!regexp","value":"\\d"}]'>
        <option disabled selected value> -- Выберите -- </option>
        <?php foreach ($data['couriers'] as $courier) {
            echo '<option value='.$courier['id'].'>'.$courier['name'].'</option>';
        }?>
    </select>
  </div>
    <div class="form-group" id="result">
    </div>
  
</form>