<?php

/**
 * Description of IndexController
 *
 * @author titovskiy
 */
use vi\View;
use vi\controllers\BaseController;
use vi\models\Region;
use vi\models\BaseModel;

class IndexController extends BaseController
{
    
    public function indexAction() {
       $this->view->generate('index');
    }
    
    public function notFoundAction() {
        $this->view->generate('not-found',[
            'title' => 'ОШИБКА',
        ]);
    }
    
    public function getScheduleAction()
    {
        if (!empty($this->post) && $this->post['access']){
            $datetimeFrom = strtotime($this->post['dateFrom'].$this->post['timeFrom']);
            $datetimeTo = strtotime($this->post['dateTo'].$this->post['timeTo']);
            $sql = 'SELECT s.`datetime_to`,
                           s.`datetime_from`,
                           c.`name` as cname,
                           r.`name`,
                           r.`to_from`
                    FROM `schedule` as s 
                    LEFT JOIN `courier` as c ON s.`courier_id` = c.id
                    LEFT JOIN `region` as r ON s.`region_id` = r.id
                    WHERE  s.region_id='.$this->post['region'] .
                        ' AND s.`datetime_to` <='.$datetimeTo .
                        ' AND s.`datetime_to`>='.$datetimeFrom;
            $result = BaseModel::execSql($sql);
            if ($result && !empty($result)) {
                $row = '';
                foreach ($result as $key => $value) {
                    $row .= '<tr><td>'.$value['name'].'</td>'.
                            '<td>'.date("Y-m-d H:i:s", $value['datetime_to']).'</td>'.
                            '<td>'.date("Y-m-d H:i:s", $value['datetime_to']+$value['to_from']*60*60).'</td>'.
                            '<td>'.$value['cname'].'</td>'.
                            '<td>'.date("Y-m-d H:i:s", $value['datetime_from']).'</td></tr>';
                }
                echo "<table class='table'>
                    <tr>
                    <th>Место назначения</th>
                    <th>Время отправления</th>
                    <th>Время прибытия</th>
                    <th>Курьер</th>
                    <th>Время возврата в Москву</th>
                    </tr>
                    ".$row.'</table>';
                    
                
            } else {
                $result = "<div class='alert alert-danger'><p>Данные не найдены</p></div>";
                echo $result;
            }
        } else header('location: /not-foud');
    }
    
    public function scheduleAction()
    {
        $regions = BaseModel::getAll('region');
        $js = '<script>
                $("#form-schedule").lemongrab();
                document.querySelector(\'#form-schedule\').addEventListener(\'input\',function(){
                    if ($("#form-schedule").isNotRequired()) {
                        $.ajax({
                            type: "POST",
                            url: "/getSchedule",
                            data: {
                                "access" : true,
                                "dateFrom" : $("#dateFrom").val(),
                                "timeFrom" : $("#timeFrom").val(),
                                "dateTo" : $("#dateTo").val(),
                                "timeTo" : $("#timeTo").val(),
                                "region" : $("#region").val(),
                                
                            },
                            success: function(data) {
                                $("#result").html(data);
                            },
                        });
                    return false;
                    }
                });
                </script>';
        return $this->view->generate('schedule',[
            'title' => 'Расписание поездок',
            'regions' => $regions,
            'js' => $js,
        ]);
    }
    
    public function checkCourierAction()
    {
        if (!empty($this->post) && $this->post['access']){
            $date = $this->post['date'];
            $time = $this->post['time'];
            $region = $this->post['region'];
            $regionName = BaseModel::getFromBy('name', 'region', 
                    ['`id`' => '='.$region,]);
            $courier = $this->post['courier'];
            $datetime = strtotime($date.$time);
            $check = BaseModel::getFromBy('`datetime_to`, `datetime_from`', 'schedule', 
                    [
                        '`courier_id`' => '='.$courier,
                        ' AND `datetime_to`' => '<='.$datetime,
                        ' AND `datetime_from`' => '>='.$datetime,
                    ]);
            if (!$check) {
                $datetime_region = BaseModel::getFromBy('to_from', 'region', ['id' => '='.$region]);
                $date_arrival = 
                $date_arrival = date("Y-m-d H:i:s", $datetime + intval($datetime_region[0]['to_from'])*60*60);
                echo "<div class='alert alert-success'><p>$date $time курьер свободен!"
                        . "<b> Прибытие в ".$regionName[0]['name']." - $date_arrival</b> </p></div>"
                . "<button type='submit' name='save' class='btn btn-success'>Добавить</button>";
            } else {
                echo "<div class='alert alert-danger'><p>Курьер занят!</p></div>";
            }
        } else header('location: /not-foud');
    }
    
    public function formAction(){
        if (!empty($this->post) && isset($this->post['save'])) {
            $date = $this->post['date'];
            $time = $this->post['time'];
            $courier = $this->post['courier'];
            $region = $this->post['region'];
            $datetime_to = strtotime($date.$time);
            $datetime_region = BaseModel::getFromBy('to_from', 'region', ['id' => '='.$region]);
            $datetime_from = $datetime_to + intval($datetime_region[0]['to_from'])*60*60*2;
            $check = BaseModel::getFromBy('`datetime_to`, `datetime_from`', 'schedule', 
                [
                    '`courier_id`' => '='.$courier,
                    ' AND `datetime_to`' => '<='.$datetime_to,
                    ' AND `datetime_from`' => '>='.$datetime_to,
                ]);
            if (!$check) {
                if (BaseModel::save('schedule', [
                    'courier_id' => $courier,
                    'region_id' => $region,
                    'datetime_to' => $datetime_to,
                    'datetime_from' => $datetime_from,
                ])){
                    $resultSave = "<div class='alert alert-success'><p>Данные сохранены</p></div>";
                } else {
                    $resultSave = "<div class='alert alert-danger'><p>Данные не сохранены! Ошибка!</p></div>";
                } 
            } else {
                $resultSave = "<div class='alert alert-danger'><p>Данные не сохранены! Ошибка!</p></div>";
            }
        }
        $js = '<script>
                $("#form").lemongrab();
                document.querySelector(\'form\').addEventListener(\'input\',function(){
                    if ($("#form").isNotRequired()) {
                        $.ajax({
                            type: "POST",
                            url: "/check-courier",
                            data: {
                                "access" : true,
                                "courier" : $("#courier").val(),
                                "date" : $("#date").val(),
                                "time" : $("#time").val(),
                                "region" : $("#region").val(),
                                
                            },
                            success: function(data) {
                                $("#result").html(data);
                            },
                        });
                    return false;
                    }
                });
                </script>';
        $regions = BaseModel::getAll('region');
        $couriers = BaseModel::getAll('courier');
        $this->view->generate('form',[
            'title' => 'Форма для внесения данных',
            'regions' => $regions,
            'couriers' => $couriers,
            'js' => $js,
            'resultSave' => isset($resultSave) ? $resultSave : null,
        ]);
    }
    
    public function uploadAction()
    {
        if (isset($this->post['save'])){
            if($_FILES["filename"]["size"] > 1024*3*1024)
            {
              echo ("Размер файла превышает три мегабайта");
              exit;
            }
            // Проверяем загружен ли файл
            if(is_uploaded_file($_FILES["filename"]["tmp_name"]))
            {
              // Если файл загружен успешно, перемещаем его
              // из временной директории в конечную
              move_uploaded_file($_FILES["filename"]["tmp_name"], "file".".csv");
            } else {
               echo("Ошибка загрузки файла");

           }
            $nameFile="file.csv";
            $separator=";";
            $fop = fopen($nameFile , "r+");
            $i=0;
            while (!feof($fop))
            {
                $read = fgets($fop, 3000);                
                list($date_to, $courier, $region) = explode($separator ,$read);
                $courier_id = BaseModel::getFromBy('`id`', '`courier`', ['`name`' => "='$courier'"]);
                $regionArr = BaseModel::getFromBy('`id`, `to_from`', '`region`', ['`name`' => "='$region'"]);
                $date_from = $regionArr['0']['to_from']*60*60 + strtotime($date_to);
                $sql = "INSERT INTO `schedule` SET 
                        `courier_id`=".$courier_id[0]['id'].",
                        `region_id`=".$regionArr[0]['id'].",
                        `datetime_to`=".strtotime($date_to).",   
                        `datetime_from`=$date_from";
                if (BaseModel::execInsertSql($sql)){
                    $i++;
                }
            }
            fclose($fop);
            $res =  '<p class="t">Импортировано в базу записей: '.$i.'</p>';
            
        } 
            $this->view->generate('upload',[
                'title' => 'Загрузка данных',
                'resultSave' => isset($res) ? $res : null,
            ]);
    }

}
