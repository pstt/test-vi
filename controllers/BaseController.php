<?php

namespace vi\controllers;

/**
 * Description of BaseConroller
 *
 * @author titovskiy
 */
use vi\View;

class BaseController {
    
    public $view;
    public $post = [];
    function __construct() {
       $this->view = new View();
       if (isset($_POST)) {
           foreach ($_POST as $key => $value) {
               $this->post[$key] = htmlspecialchars($value);
           }
       }
    }
    
}
